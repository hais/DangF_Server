package com.dangf.launcher.server.app

import android.os.Bundle
import android.view.WindowManager
import com.dangf.launcher.server.R
import pw.hais.utils_lib.app.AppBaseActivity


/**
 * Created by hais1992 on 2017/5/19.
 */
abstract class BaseActivity(var layout: Int) : AppBaseActivity(layout) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //防止休眠,设置全屏,设置无导航栏
        window.setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out)
    }

}