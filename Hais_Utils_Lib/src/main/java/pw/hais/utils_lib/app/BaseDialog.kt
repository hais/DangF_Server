package pw.hais.utils_lib.app

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import pw.hais.utils_lib.R

/**
 *
 * Created by hais1992 on 2017/5/20.
 */
abstract class BaseDialog(var activity: Activity, var layoutId: Int, val cancelable: Boolean = true) : Dialog(activity, R.style.custom_dialog) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val views = View.inflate(activity, layoutId, null)
        setContentView(views, LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT))
        setCanceledOnTouchOutside(cancelable)
        setCancelable(cancelable)  //false不可以用“返回键”取消	true可用“返回键”取消
        views.isFocusable = true
        initView(views)
    }

    abstract fun initView(view: View)
}
