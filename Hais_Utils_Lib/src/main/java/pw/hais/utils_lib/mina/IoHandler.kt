package pw.hais.utils_lib.mina

import org.apache.mina.core.service.IoHandlerAdapter
import org.apache.mina.core.session.IoSession
import pw.hais.utils_lib.utils.L


class IoHandler(var listener: OnSockectListener?) : IoHandlerAdapter() {

    @Throws(Exception::class)
    override fun sessionOpened(session: IoSession?) {
        listener?.onSessionOpened()
    }

    @Throws(Exception::class)
    override fun sessionClosed(session: IoSession?) {
        listener?.onSessionClosed()
    }

    @Throws(Exception::class)
    override fun messageReceived(session: IoSession?, message: Any?) {
        L.e("收到消息：$message")
        listener?.onMessageReceived(message.toString())
    }

    override fun exceptionCaught(session: IoSession?, cause: Throwable?) {
        L.e("连接中断")
    }

}