package pw.hais.utils_lib.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by hais1992 on 2017/5/24.
 */
@SuppressLint("WrongConstant")
object DateUtils {

    /**
     * 當月的第一天
     */
    open fun getStartTimeLog(): Long {
//        val dateStr = SimpleDateFormat("yyyy-MM-01", Locale.CHINA).format(time)
//        val dateLong = SimpleDateFormat("yyyy-MM-dd", Locale.CHINA).parse(dateStr)
//        return dateLong.time

//        val calendar = Calendar.getInstance()
//        calendar.time = Date(time)
//        calendar.set(Calendar.DAY_OF_MONTH, 1)
//        return calendar.time.time

        val time = System.currentTimeMillis()
        val calendar = Calendar.getInstance()
        calendar.time = Date(time)

        val dateStr = SimpleDateFormat("yyyy-MM-01 00:00:00", Locale.CHINA).format(time)
        val dateLong = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA).parse(dateStr)
        return dateLong.time
    }

    /**
     * 當月的最後一天
     */
    open fun getEndTimeLog(): Long {
        val time = System.currentTimeMillis()
        val calendar = Calendar.getInstance()
        calendar.time = Date(time)
        val lastDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH)

        val dateStr = SimpleDateFormat("yyyy-MM-$lastDay 23:59:59", Locale.CHINA).format(time)
        val dateLong = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA).parse(dateStr)
        return dateLong.time
    }

    /**
     * 獲取指定格式的時間
     */
    open fun getStrTime(timeTemp: String = "yyyy-MM-dd"): String {
        val dateStr = SimpleDateFormat(timeTemp, Locale.CHINA).format(System.currentTimeMillis())
        return dateStr
    }

    /**
     * 獲取月份
     */
    open fun getMonth(timeTemp: String = "MM"): String {
        val dateStr = SimpleDateFormat(timeTemp, Locale.CHINA).format(System.currentTimeMillis())
        return dateStr
    }

}