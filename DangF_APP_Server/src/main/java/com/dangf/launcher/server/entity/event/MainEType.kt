package com.dangf.app.launcher.entity.event

/**
 * Created by hais1992 on 2017/9/15.
 */
enum class MainEType {
    BrightnessAdjustmentWindows, //亮度调节
    OutboardMotorWindows, //挂机锁
    RefreshAppList, //刷新软件列表
    ShowToast, //显示吐司
    RefreshLog, //清除游戏数据
    FileSelectResule, //清除游戏数据
}