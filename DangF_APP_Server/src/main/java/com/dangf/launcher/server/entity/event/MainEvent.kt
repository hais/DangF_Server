package com.dangf.app.launcher.entity.event

import org.greenrobot.eventbus.EventBus

/**
 * Created by hais1992 on 2017/5/19.
 */
class MainEvent(var type: MainEType, var obj: Any = "") {

    companion object {
        fun post(event: MainEType, obj: Any = "") {
            EventBus.getDefault().post(MainEvent(event, obj))
        }
    }
}
