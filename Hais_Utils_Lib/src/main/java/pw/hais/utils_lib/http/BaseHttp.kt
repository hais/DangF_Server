package pw.hais.utils_lib.http

import com.dangf.app.launcher.http.JavaBeanRequest
import com.yanzhenjie.nohttp.NoHttp
import com.yanzhenjie.nohttp.RequestMethod
import com.yanzhenjie.nohttp.rest.OnResponseListener
import com.yanzhenjie.nohttp.rest.Response


/**
 * Created by 管理员 on 2017/9/12.
 */
object BaseHttp {


    /**
     * 开始一次请求
     * @what 标识
     * @url 请求地址
     * @requestMethond 请求类型
     * @header 请求头支持添加各种类型，比如String、int、long、double、float等等
     * @params 参数支持添加各种类型，比如Binary、File、String、int、long、double、float等等
     * @listener 回调监听
     */
    inline fun <reified T> request(what: Int, url: String, requestMethod: RequestMethod, header: Map<String, String>?, params: Map<String, Any>?, listener: OnHttpListener<T>) {
        val request = JavaBeanRequest(url, requestMethod, T::class.java)
        header?.forEach { request.addHeader(it.key, it.value) } //处理Header
        request.add(params)


        NoHttp.getRequestQueueInstance().add(what, request, object : OnResponseListener<T> {
            override fun onStart(what: Int) {
                listener.onStart(what)
            }

            override fun onFinish(what: Int) {
                listener.onFinish(what)
            }

            override fun onFailed(what: Int, response: Response<T>?) {
                listener.onFailed(what, response, response?.exception)
            }

            override fun onSucceed(what: Int, response: Response<T>?) {
                listener.onSuccess(what, response, response?.get()!!)
            }

        })
    }

}