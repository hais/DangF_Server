package pw.hais.utils_lib.utils

import android.content.Context
import android.util.Log
import android.widget.Toast
import pw.hais.utils_lib.app.App
import pw.hais.utils_lib.app.Config

/**
 * Created by hais1992 on 2017/5/19.
 */
object L {
    /**
     * 打印Log.i
     * @msg 打印的内容
     * @tag 打印的标签，可为空
     */
    fun i(msg: String, tag: String = Config.LogTag) {
        if (Config.DeBug) Log.i(tag, msg)
    }

    /**
     * 打印Log.d
     * @msg 打印的内容
     * @tag 打印的标签，可为空
     */
    fun d(msg: String, tag: String = Config.LogTag) {
        if (Config.DeBug) Log.d(tag, msg)
    }

    /**
     * 打印Log.v
     * @msg 打印的内容
     * @tag 打印的标签，可为空
     */
    fun v(msg: String, tag: String = Config.LogTag) {
        if (Config.DeBug) Log.v(tag, msg)
    }

    /**
     * 打印Log.w
     * @msg 打印的内容
     * @tag 打印的标签，可为空
     */
    fun w(msg: String, tag: String = Config.LogTag) {
        if (Config.DeBug) Log.w(tag, msg)
    }

    /**
     * 打印Log.e
     * @msg 打印的内容
     * @tag 打印的标签，可为空
     * @ex  打印异常，可为空
     */
    fun e(msg: String, tag: String = Config.LogTag, ex: Throwable? = null) {
        if (Config.DeBug) Log.e(tag, msg, ex)
    }


    /*--------------------------------------Toast输出-----------------------------------------*/


    /**
     * 显示Toast.SHORT
     * @message 显示的消息
     * @context 可选，上下文
     */
    fun showShort(message: CharSequence, context: Context? = App.context) {
        Toast.makeText(context!!, message, Toast.LENGTH_SHORT).show()
    }

    /**
     * 显示Toast.LONG
     * @message 显示的消息
     * @context 可选，上下文
     */
    fun showLong(message: CharSequence, context: Context? = App.context) {
        Toast.makeText(context!!, message, Toast.LENGTH_LONG).show()
    }

}