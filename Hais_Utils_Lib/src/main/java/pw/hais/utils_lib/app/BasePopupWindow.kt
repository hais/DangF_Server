package pw.hais.utils_lib.app

import android.app.Activity
import android.graphics.drawable.ColorDrawable
import android.support.annotation.LayoutRes
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow

/**
 * Created by hais1992 on 2017/5/20.
 */
abstract class BasePopupWindow(context: Activity?, @LayoutRes layout: Int) : PopupWindow(context) {
    val activity: Activity by lazy { context as Activity }

    init {
        width = ViewGroup.LayoutParams.MATCH_PARENT
        height = ViewGroup.LayoutParams.MATCH_PARENT
        contentView = LayoutInflater.from(context).inflate(layout, null)
        isFocusable = true  //获得焦点
        isTouchable = true  //获得触摸
        isOutsideTouchable = true   //空白处退出
        setBackgroundDrawable(ColorDrawable(0xC8000000.toInt()))
        initView(contentView)
    }

    abstract fun initView(v: View)



    open fun showContent() {
        showContent(activity.window.decorView)
    }

    open fun showContent(view: View) {
        contentView.isFocusable = true //监听按键
        showAtLocation(view, Gravity.CENTER_VERTICAL, 0, 0)
    }

}