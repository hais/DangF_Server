package com.dangf.launcher.server.activity

import android.content.Intent
import android.view.View
import com.dangf.launcher.server.R
import com.dangf.launcher.server.app.BaseActivity
import com.dangf.launcher.server.pulgin.store.Store
import kotlinx.android.synthetic.main.activity_register.*
import pw.hais.utils_lib.utils.L

/**
 * 安装
 * Created by hais1992 on 2017/9/27.
 */
class InstallActivity : BaseActivity(R.layout.activity_register), View.OnClickListener {
    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.btn_close -> finish()
            R.id.btn_install -> doInstall()
        }
    }

    override fun onInitViewsAndData() {
        btn_close.setOnClickListener(this)
        btn_install.setOnClickListener(this)
    }

    private fun doInstall() {
        val storeName = edit_name.text.toString()
        val timePrice = edit_time.text.toString()
        val userName = edit_username.text.toString()
        val password = edit_password.text.toString()
        if (storeName.isEmpty() || timePrice.isEmpty() || userName.isEmpty() || password.isEmpty()) L.showShort("信息不完整，安装失败！")
        else {
            L.showShort("注册成功，正在进入程序！")
            val store = Store(storeName, userName, password, "", timePrice.toInt())
            store.save()
            startActivity(Intent(context, InitActivity::class.java))
            finish()
        }
    }

}