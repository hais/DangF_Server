package com.dangf.app.launcher.entity.event

import org.greenrobot.eventbus.EventBus

/**
 * Created by hais1992 on 2017/5/19.
 */
class IOEvent(var type: IOEType, var obj: Any = "") {

    companion object {
        fun post(event: IOEType, obj: Any = "") {
            EventBus.getDefault().post(IOEvent(event, obj))
        }
    }
}