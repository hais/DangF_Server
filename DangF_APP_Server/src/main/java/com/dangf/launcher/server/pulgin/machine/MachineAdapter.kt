package com.dangf.launcher.server.pulgin.machine

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.dangf.launcher.server.R

/**
 * Created by hais1992 on 2017/9/29.
 */
class MachineAdapter(var list: List<UsageLog>) : BaseQuickAdapter<UsageLog, BaseViewHolder>(R.layout.machine_manage_fragment_item, list) {
    override fun convert(helper: BaseViewHolder?, item: UsageLog?) {
        val sort = (list.indexOf(item) + 1)
        helper?.setText(R.id.text_sort, sort.toString())
        helper?.setText(R.id.text_name, item?.machineName)
        helper?.setText(R.id.text_startTime, item?.startTime.toString())
        helper?.setText(R.id.text_endTime, item?.endTime.toString())
        helper?.setText(R.id.text_process, item?.process)
        helper?.setText(R.id.text_status, item?.status.toString())
        if (sort % 2 == 0) helper?.setBackgroundColor(R.id.layout_item, (0xFFF5F8FA).toInt())
        else helper?.setBackgroundRes(R.id.layout_item, R.drawable.background_stroke_solid_icons_secondary)
    }
}