package pw.hais.utils_lib.http

import com.yanzhenjie.nohttp.RequestMethod

/**
 * Created by 管理员 on 2017/9/12.
 */
object Http {

    inline fun <reified T> get(what: Int, url: String, params: Map<String, Any>?, listener: OnHttpListener<T>) {
        BaseHttp.request(what, url, RequestMethod.GET, null, params, listener)
    }

    inline fun <reified T> post(what: Int, url: String, params: Map<String, Any>?, listener: OnHttpListener<T>) {
        BaseHttp.request(what, url, RequestMethod.POST, null, params, listener)
    }

}