package pw.hais.utils_lib.mina

/**
 * Created by cuihp on 2017/4/15.
 */

data class SockectConfig(var address: String? = null,   //地址
                         var port: Int = 44451,         //端口号
                         var kafiTime: Int = 10,        //心跳时间
                         var kafiFlag: String = "111",   //心跳发送符号
                         var outTime: Long = 20000,     //连接超时
                         var readBufferSize: Int = 1024 * 10)
