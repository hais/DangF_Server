package com.dangf.launcher.server.pulgin.advert

import com.dangf.launcher.server.httpserver.BaseDao
import com.dangf.launcher.server.httpserver.RequestModel
import org.litepal.crud.DataSupport

/**
 * Created by hais1992 on 2017/9/29.
 */
object AdvertDao : BaseDao() {

    //获取最后一条公告
    fun getLastAdvert(): Advert {
        return DataSupport.findLast(Advert::class.java)
    }

    //获取所有公告
    fun getAllAdvert(): List<Advert> {
        return DataSupport.findAll(Advert::class.java)
    }

    //处理Api请求
    fun doApi(a: String): String {
        when (a) {
            "last" -> return getJson(100, "", getLastAdvert())
            "all" -> return getJson(100, "", getAllAdvert())
            else -> return getJson(404, "接口不存在", "")
        }
    }
}