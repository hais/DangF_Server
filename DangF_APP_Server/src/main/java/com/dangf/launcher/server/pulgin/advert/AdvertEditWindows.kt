package com.dangf.launcher.server.pulgin.advert

import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.*
import com.dangf.launcher.server.R
import pw.hais.utils_lib.app.BasePopupWindow
import pw.hais.utils_lib.utils.L

/**
 * Created by hais1992 on 2017/9/29.
 */
class AdvertEditWindows(var fragment: AdvertManageFragment) : BasePopupWindow(fragment.activity, R.layout.advert_manage_edit_windows), AdapterView.OnItemSelectedListener {
    private var text_delete: TextView? = null
    private var text_close: TextView? = null
    private var text_lable: TextView? = null
    private var edit_title: EditText? = null
    private var spinner_type: Spinner? = null
    private var spinner_position: Spinner? = null
    private var edit_content: EditText? = null
    private var btn_ok: Button? = null
    private var checkbox_status: CheckBox? = null
    private var item: Advert? = null

    override fun initView(v: View) {
        text_delete = v.findViewById(R.id.text_delete)
        checkbox_status = v.findViewById(R.id.checkbox_status)
        text_lable = v.findViewById(R.id.text_lable)
        edit_title = v.findViewById(R.id.edit_title)
        spinner_type = v.findViewById(R.id.spinner_type)
        spinner_position = v.findViewById(R.id.spinner_position)
        edit_content = v.findViewById(R.id.edit_content)
        text_close = v.findViewById(R.id.text_close)
        text_close?.setOnClickListener { dismiss() }
        btn_ok = v.findViewById(R.id.btn_ok)
        btn_ok?.setOnClickListener { saveAdvert() }
        text_delete?.setOnClickListener {
            AlertDialog.Builder(activity).setTitle("删除").setMessage("您是否要删除此条记录？删除后将无法恢复").setPositiveButton("删除") { _, _ ->
                item?.delete()
                L.showShort("数据已删除")
                dismiss()
                fragment.onRefreshData()
            }.setNegativeButton("取消", null).show()
        }

        spinner_type?.onItemSelectedListener = this
        spinner_position?.onItemSelectedListener = this
    }

    override fun showContent() {
        item = null
        edit_title?.setText("")
        edit_content?.setText("")
        text_delete?.visibility = View.GONE
        super.showContent()
    }

    fun showContent(item: Advert) {
        this.item = item
        text_lable?.text = "编辑公告"
        edit_title?.setText(item.title)
        edit_content?.setText(item.content)
        text_delete?.visibility = View.VISIBLE
        if (item.status == "禁用") checkbox_status?.isChecked = false
        super.showContent()
    }

    private fun saveAdvert() {
        if (edit_title!!.text.isEmpty() || edit_content!!.text.isEmpty()) L.showShort("信息不完整！")
        else if (item == null) {
            Advert(edit_title!!.text.toString(), "$itemType", "$itemPosition", if (checkbox_status!!.isChecked) "启用" else "禁用", edit_content!!.text.toString()).save()
            L.showShort("发布成功！")
            fragment.onRefreshData()
            dismiss()
        } else {
            item?.title = edit_title!!.text.toString()
            item?.content = edit_content!!.text.toString()
            item?.type = itemType + ""
            item?.position = itemPosition + ""
            item?.status = if (checkbox_status!!.isChecked) "启用" else "禁用"
            item?.save()
            L.showShort("修改成功！")
            fragment.onRefreshData()
            dismiss()
        }
    }


    val typeItems = fragment.resources.getStringArray(R.array.advert_type)
    val positionItems = fragment.resources.getStringArray(R.array.advert_position)
    private var itemType: String? = null
    private var itemPosition: String? = null
    override fun onNothingSelected(p0: AdapterView<*>?) {

    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        if (p0 == spinner_type) itemType = typeItems[p2]
        else if (p0 == spinner_position) itemPosition = positionItems[p2]
    }
}