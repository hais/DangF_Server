package com.dangf.launcher.server.pulgin.soft

import org.litepal.annotation.Column
import org.litepal.crud.DataSupport

/**
 * 软件
 * Created by hais1992 on 2017/9/27.
 */
data class Soft(
        var name: String? = "",           //软件名称
        @Column(unique = true)var packageName: String? = "",    //包名
        var icon: String? = "",           //软件图标
        var type: String? = "",           //类型
        var status: String? = "",         //状态
        var apkUrl: String? = "",         //软件下载地址
        var dataUrl: String? = "",        //数据下载地址
        var version: String? = "",        //软件版本
        var message: String? = ""         //信息
) : DataSupport()