package com.dangf.launcher.server.httpserver

import android.text.TextUtils
import com.yanzhenjie.andserver.RequestHandler
import com.yanzhenjie.andserver.handler.StorageRequestHandler
import com.yanzhenjie.andserver.util.StorageWrapper
import com.yanzhenjie.andserver.website.BasicWebsite

/**
 *
 * The web site in storage.
 * Created by Yan Zhenjie on 2017/3/15.
 */

class DFStorageWebsite(rootPath: String) : BasicWebsite(rootPath) {
    companion object {
        var rootPath = "/"
    }

    private val mStorageWrapper: StorageWrapper
    private val mRootPath: String

    init {
        if (TextUtils.isEmpty(rootPath)) throw NullPointerException("The RootPath can not be null.")
        DFStorageWebsite.rootPath = rootPath
        this.mRootPath = BasicWebsite.trimSlash(rootPath)
        mStorageWrapper = StorageWrapper()
    }

    override fun onRegister(handlerMap: MutableMap<String, RequestHandler>) {
        val indexHandler = StorageRequestHandler(INDEX_HTML)
        handlerMap.put("", indexHandler)
//        handlerMap.put(mRootPath, indexHandler)
//        handlerMap.put(mRootPath + File.separator, indexHandler)
//        handlerMap.put(mRootPath + File.separator + INDEX_HTML, indexHandler)

        val pathList = mStorageWrapper.scanFile(BasicWebsite.getHttpPath(mRootPath))
        for (path in pathList) {
            val requestHandler = StorageRequestHandler(path)
            handlerMap.put(path.replace(mRootPath + "/", ""), requestHandler)
        }
    }

}
