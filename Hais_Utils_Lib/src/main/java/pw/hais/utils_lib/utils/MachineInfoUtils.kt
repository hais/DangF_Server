package pw.hais.utils_lib.utils

/**
 * APP信息活取
 * Created by hais1992 on 15-4-24.
 */
object MachineInfoUtils {

    //获取用户 mac 地址
    val macAddress: String
        get() {
            return ShellUtils.execCmd("cat /sys/class/net/wlan0/address", true).successMsg
        }

    //获取系统版本
    val systemVersion: String
        get() {
            return android.os.Build.VERSION.RELEASE
        }

    //获取手机型号
    val productType: String
        get() {
            return android.os.Build.MODEL
        }

}