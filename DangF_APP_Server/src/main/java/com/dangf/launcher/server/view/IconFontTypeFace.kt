package com.dangf.launcher.server.view

import android.content.Context
import android.graphics.Typeface

/**
 * Created by hais1992 on 2017/10/1.
 */

object IconFontTypeFace {

    //用static,整个app共用整个typeface就够了
    private var ttfTypeface: Typeface? = null

    @Synchronized internal fun getTypeface(context: Context): Typeface? {
        if (ttfTypeface == null) {
            try {
                ttfTypeface = Typeface.createFromAsset(context.assets, "iconfont.ttf")
            } catch (e: Exception) {

            }

        }
        return ttfTypeface
    }

    @Synchronized
    fun clearTypeface() {
        ttfTypeface = null
    }
}