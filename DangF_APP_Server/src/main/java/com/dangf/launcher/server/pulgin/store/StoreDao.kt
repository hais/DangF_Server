package com.dangf.launcher.server.pulgin.store

import com.dangf.launcher.server.httpserver.BaseDao
import org.litepal.crud.DataSupport

/**
 * 商家信息
 * Created by hais1992 on 2017/9/29.
 */
object StoreDao : BaseDao() {

    //根据ID获取机器信息
    fun getStoreInfo(): Store? {
        try {
            return DataSupport.findAll(Store::class.java).last()
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }
    }

    //处理Api请求
    fun doApi(a: String): String {
        when (a) {
            "info" -> {
                val store = getStoreInfo()
                if (store == null) {
                    return getJson(110, "数据获取出错！", null)
                } else {
                    return getJson(100, "", store)
                }
            }
            else -> return getJson(404, "接口不存在！", null)
        }
        return ""
    }
}