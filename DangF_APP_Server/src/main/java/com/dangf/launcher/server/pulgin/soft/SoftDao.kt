package com.dangf.launcher.server.pulgin.soft

import com.dangf.launcher.server.httpserver.BaseDao
import org.litepal.crud.DataSupport

/**
 * Created by hais1992 on 2017/9/29.
 */
object SoftDao : BaseDao() {

    fun getAllSoft(): List<Soft> {
        return DataSupport.findAll(Soft::class.java)
    }

    fun getUserSoft(): List<Soft> {
        return DataSupport.where("type = '用户软件' and status = '启用'").find(Soft::class.java)
    }

    fun getAdminSoft(): List<Soft> {
        return DataSupport.where("type = '管理工具' and status = '启用'").find(Soft::class.java)
    }

    fun doApi(a: String): String {
        when (a) {
            "all" -> return getJson(100, "", getAllSoft())
            "user" -> return getJson(100, "", getUserSoft())
            "admin" -> return getJson(100, "", getAdminSoft())
            else -> return getJson(404, "接口不存在", "")
        }
    }
}