package pw.hais.utils_lib.http

import com.yanzhenjie.nohttp.rest.Response

/**
 * Created by 管理员 on 2017/9/12.
 */
interface OnHttpListener<T> {
    fun onSuccess(what: Int, response: Response<T>?, data: T)


    fun onFailed(what: Int, response: Response<T>?, e: Exception?) {
    }

    fun onStart(what: Int) {

    }

    fun onFinish(what: Int) {

    }
}