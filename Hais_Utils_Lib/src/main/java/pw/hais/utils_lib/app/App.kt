package pw.hais.utils_lib.app

import android.app.Application
import com.bulong.rudeness.RudenessScreenHelper
import com.yanzhenjie.nohttp.Logger
import com.yanzhenjie.nohttp.NoHttp


open class App : Application() {

    companion object {
        var context: App? = null
    }

    override fun onCreate() {
        super.onCreate()

        context = this
        NoHttp.initialize(context)
        Logger.setDebug(Config.DeBug) // 开启NoHttp调试模式。
        Logger.setTag(Config.LogTag) // 设置NoHttp打印Log的TAG。

        //dpi=1080,尺寸=30.6,1080x1920 , 布局以pt作为单位
        //尺寸=23.5,1440x900 , 布局以pt作为单位
//        RudenessScreenHelper(this, 1920f).activate()
    }

}