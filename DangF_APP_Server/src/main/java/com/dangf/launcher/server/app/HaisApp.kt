package com.dangf.launcher.server.app

import org.litepal.LitePal
import pw.hais.utils_lib.app.App

/**
 * Created by hais1992 on 2017/9/27.
 */
class HaisApp : App() {
    override fun onCreate() {
        super.onCreate()
        LitePal.initialize(this)
    }
}