package com.dangf.launcher.server.pulgin.advert

import org.litepal.crud.DataSupport

/**
 * 广告、公告
 * Created by hais1992 on 2017/9/27.
 */
data class Advert(
        var title: String = "",       //标题
        var type: String = "",        //类型
        var position: String = "",    //位置
        var status: String = "",      //状态
        var content: String = ""      //内容详情
) : DataSupport()