package com.dangf.launcher.server.pulgin.advert

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.dangf.launcher.server.R
import com.dangf.launcher.server.app.BaseFragment
import kotlinx.android.synthetic.main.advert_manage_fragment.*
import org.litepal.crud.DataSupport

/**
 * 广告管理
 * Created by hais1992 on 2017/9/27.
 */
class AdvertManageFragment : BaseFragment(R.layout.advert_manage_fragment), BaseQuickAdapter.OnItemClickListener {
    val advertEditWindows by lazy { AdvertEditWindows(this) }

    override fun onInitViewsAndData(v: View?) {
        text_advert_post.setOnClickListener({ advertEditWindows.showContent() })
        onRefreshData()
    }

    override fun onItemClick(adapter: BaseQuickAdapter<*, *>?, view: View?, position: Int) {
        val item = adapter?.getItem(position) as Advert
        advertEditWindows.showContent(item)
    }

    //刷新数据
    fun onRefreshData() {
        val data_list = DataSupport.findAll(Advert::class.java)
        val adpaert = AdvertAdapter(data_list)
        recycler_advert.layoutManager = LinearLayoutManager(activity)
        recycler_advert.adapter = adpaert
        adpaert.onItemClickListener = this
    }
}