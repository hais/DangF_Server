package com.dangf.launcher.server.httpserver

import com.alibaba.fastjson.JSONObject
import java.net.URLDecoder

/**
 * Created by hais1992 on 2017/9/29.
 */
open class BaseDao {

    fun decode(str: String?): String {
        val o = URLDecoder.decode(str, "utf-8")
        return URLDecoder.decode(o, "utf-8")
    }

    fun getJson(status: Int, message: String, any: Any?): String {
        return JSONObject.toJSONString(RequestModel(status, message, any))
    }
}