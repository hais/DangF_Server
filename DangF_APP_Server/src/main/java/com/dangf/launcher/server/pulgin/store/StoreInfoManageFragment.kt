package com.dangf.launcher.server.pulgin.store

import android.view.View
import android.widget.TextView
import com.dangf.app.launcher.entity.event.MainEType
import com.dangf.app.launcher.entity.event.MainEvent
import com.dangf.launcher.server.R
import com.dangf.launcher.server.app.BaseFragment
import com.dangf.launcher.server.app.V
import kotlinx.android.synthetic.main.store_manage_fragment.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import pw.hais.utils_lib.utils.L

/**
 * 系统管理
 * Created by hais1992 on 2017/9/27.
 */
class StoreInfoManageFragment : BaseFragment(R.layout.store_manage_fragment), View.OnClickListener {

    override fun onInitViewsAndData(v: View?) {
        text_menu_log.setOnClickListener(this)
        text_menu_setting.setOnClickListener(this)
        text_menu_security.setOnClickListener(this)

        edit_store_name.setText(V.store?.name)
        edit_wallpaper.setText(V.store?.wallpaper)
        edit_time.setText(V.store?.timePrice.toString())
        btn_setting_base_ok.setOnClickListener(this)

        edit_username.setText(V.store?.username)
        btn_setting_security_ok.setOnClickListener(this)
        text_log.text = V.runLog.toString()

        text_menu_log.performClick()
        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this)
    }


    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.btn_setting_base_ok -> {
                if (edit_store_name.text.isEmpty() || edit_time.text.isEmpty()) L.showShort("请输入完整的信息！")
                else {
                    V.store?.name = edit_store_name.text.toString()
                    V.store?.timePrice = edit_time.text.toString().toInt()
                    V.store?.updateAll()
                    L.showShort("保存成功！")
                }
            }
            R.id.btn_setting_security_ok -> {
                if (edit_password_old.text.isEmpty() || edit_username.text.isEmpty() || edit_password.text.isEmpty()) L.showShort("请输入完整的信息！")
                else if (edit_password_old.text.toString() == V.store?.password) {
                    V.store?.username = edit_username.text.toString()
                    V.store?.password = edit_password.text.toString()
                    V.store?.updateAll()
                    L.showShort("保存成功！")
                } else L.showShort("旧密码输入出错，修改失败！")
            }
            R.id.text_menu_log, R.id.text_menu_setting, R.id.text_menu_security -> setItemColor(p0 as TextView)
        }

    }

    fun setItemColor(view: TextView) {
        //复原字体颜色
        text_menu_log.setTextColor(activity.resources.getColor(R.color.app_primary_text))
        text_menu_setting.setTextColor(activity.resources.getColor(R.color.app_primary_text))
        text_menu_security.setTextColor(activity.resources.getColor(R.color.app_primary_text))
        //复原背景色
        text_menu_log.setBackgroundColor(activity.resources.getColor(R.color.app_primary_light))
        text_menu_setting.setBackgroundColor(activity.resources.getColor(R.color.app_primary_light))
        text_menu_security.setBackgroundColor(activity.resources.getColor(R.color.app_primary_light))
        //隐藏
        layout_setting_log.visibility = View.GONE
        layout_setting_base.visibility = View.GONE
        layout_setting_security.visibility = View.GONE

        //根据要求显示
        view.setTextColor(activity.resources.getColor(R.color.app_icons))
        view.setBackgroundColor(activity.resources.getColor(R.color.app_primary_dark))
        when (view.id) {
            R.id.text_menu_log -> layout_setting_log.visibility = View.VISIBLE
            R.id.text_menu_setting -> layout_setting_base.visibility = View.VISIBLE
            R.id.text_menu_security -> layout_setting_security.visibility = View.VISIBLE
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEventMainThreadx(event: MainEvent) {
        when (event.type) {
            MainEType.RefreshLog -> {
                text_log.text = V.runLog.toString()
            }
        }
    }


}