package pw.hais.utils_lib.utils

import android.content.Context
import com.alibaba.fastjson.JSON
import pw.hais.utils_lib.app.App
import java.lang.reflect.Type

/**
 * APP缓存信息
 * @author Hello_海生
 * @date 2014年10月8日
 */
object SPUtils {
    private val context = App.context
    private val APP_ID = App.context?.packageName   //相当于文件名


    /**
     * 缓存信息
     * @param cacheName    存入名字
     * @param obj    对象
     */
    fun saveObject(cacheName: String, obj: Any) {
        try {
            val e = context!!.getSharedPreferences(APP_ID, Context.MODE_PRIVATE).edit()
            val json = JSON.toJSONString(obj)
            e.putString(cacheName, json)
            e.commit()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /**
     * 读取String缓存信息
     * @param <T>
     * @param cacheName    缓存名
     * @param classes    对象类型
    </T> */
    fun getObject(cacheName: String): String? {
        return getObject(cacheName, String::class.java, null)
    }

    /**
     * 读取缓存信息
     * @param <T>
     * @param cacheName    缓存名
     * @param classes    对象类型
    </T> */
    fun <T> getObject(cacheName: String, classes: Class<T>): T? {
        return getObject(cacheName, classes, null)
    }


    /**
     * 读取缓存信息
     * @param <T>
     * @param cacheName    缓存名
     * @param classes    对象类型
     * @param defaultValue    默认参数
    </T> */
    fun <T> getObject(cacheName: String, classes: Class<T>, defaultValue: T?): T? {
        val s = context!!.getSharedPreferences(APP_ID, Context.MODE_PRIVATE)
        val jsonString = s.getString(cacheName, "")
        if ("" != jsonString) {
            try {
                return JSON.parseObject(jsonString, classes)
            } catch (e: Exception) {
                e.printStackTrace()
                return defaultValue
            }

        }
        return defaultValue
    }

    /**
     * 读取缓存信息
     * 读取List 等列表type要这么写 new TypeToken<ArrayList></ArrayList><GoodsCategory>>(){}.getType()
     * @param <T>
     * @param cacheName    缓存名
     * @param type    对象类型
    </T></GoodsCategory> */
    fun <T> getObject(cacheName: String, type: Type): T? {
        return getObject<T>(cacheName, type, null)
    }

    /**
     * 读取缓存信息
     * @param <T>
     * @param cacheName    缓存名
     * @param type    对象类型
     * @param defaultValue    默认参数
    </T> */
    fun <T> getObject(cacheName: String, type: Type, defaultValue: T?): T? {
        val s = context!!.getSharedPreferences(APP_ID, Context.MODE_PRIVATE)
        val jsonString = s.getString(cacheName, "")
        if ("" != jsonString) {
            try {
                return JSON.parseObject(jsonString, type)
            } catch (e: Exception) {
                e.printStackTrace()
                return defaultValue
            }

        }
        return defaultValue
    }


    /**
     * 删除信息
     * @param cacheName 缓存名
     */
    fun delObject(cacheName: String) {
        val s = context!!.getSharedPreferences(APP_ID, Context.MODE_PRIVATE)
        s.edit().remove(cacheName).commit()
    }

    /**
     * 清除所有数据
     */
    fun clearAll() {
        val sp = context!!.getSharedPreferences(APP_ID, Context.MODE_PRIVATE)
        val editor = sp.edit()
        editor.clear().commit()
    }
}

