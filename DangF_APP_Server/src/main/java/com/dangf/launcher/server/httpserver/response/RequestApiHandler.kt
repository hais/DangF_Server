package com.dangf.launcher.server.httpserver.response

import com.dangf.launcher.server.app.V
import com.dangf.launcher.server.pulgin.advert.AdvertDao
import com.dangf.launcher.server.pulgin.machine.MachineDao
import com.dangf.launcher.server.pulgin.soft.SoftDao
import com.dangf.launcher.server.pulgin.store.StoreDao
import com.yanzhenjie.andserver.RequestHandler
import com.yanzhenjie.andserver.util.HttpRequestParser
import org.apache.http.HttpException
import org.apache.http.HttpRequest
import org.apache.http.HttpResponse
import org.apache.http.entity.StringEntity
import org.apache.http.protocol.HttpContext
import pw.hais.utils_lib.utils.L
import java.io.IOException
import java.net.URLDecoder

/**
 * 客户端请求接口
 * Created by Yan Zhenjie on 2016/6/13.
 */
class RequestApiHandler : RequestHandler {
    private val TAG = "HTTP服务"

    @Throws(HttpException::class, IOException::class)
    override fun handle(request: HttpRequest, response: HttpResponse, context: HttpContext) {
        V.printLog(TAG,"收到请求：${request.requestLine.uri}")
        val params = HttpRequestParser.parse(request)

        val model = URLDecoder.decode(params["m"], "utf-8")    //模块
        val action = URLDecoder.decode(params["a"], "utf-8")    //动作

        when (model) {
            "advert" -> {   //公告模块
                response.entity = StringEntity(AdvertDao.doApi(action), "utf-8")
            }
            "machine" -> {  //机器模块
                response.entity = StringEntity(MachineDao.doApi(action, params), "utf-8")
            }
            "soft" -> {     //软件模块
                response.entity = StringEntity(SoftDao.doApi(action), "utf-8")
            }
            "store" -> {    //商家模块
                response.entity = StringEntity(StoreDao.doApi(action), "utf-8")
            }
            else -> {    //其它没有的

            }
        }
    }
}