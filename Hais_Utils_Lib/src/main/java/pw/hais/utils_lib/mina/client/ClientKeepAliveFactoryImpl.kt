package pw.hais.utils_lib.mina.client

import org.apache.mina.core.session.IoSession
import org.apache.mina.filter.keepalive.KeepAliveMessageFactory
import pw.hais.utils_lib.mina.SockectConfig

/**
 * Created by hais1992 on 2017/9/26.
 */
class ClientKeepAliveFactoryImpl(val config: SockectConfig) : KeepAliveMessageFactory {

    override fun isRequest(p0: IoSession?, p1: Any?): Boolean {
        return false
    }

    override fun isResponse(p0: IoSession?, p1: Any?): Boolean {
        return config.kafiFlag == p1
    }


    override fun getRequest(p0: IoSession?): Any? {
        return config.kafiFlag
    }

    override fun getResponse(p0: IoSession?, p1: Any?): Any? {
        return null
    }

}