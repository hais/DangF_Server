package com.dangf.launcher.server.pulgin.machine

import org.litepal.annotation.Column
import org.litepal.crud.DataSupport

/**
 * 机器信息
 * Created by hais1992 on 2017/9/27.
 */
class Machine : DataSupport() {
    var name: String = ""            //名称
    var status: String = ""          //状态
    @Column(unique = true)
    var code: String = ""            //机器码
    var product: String = ""         //产品型号
    var version: String = ""         //系统版本
    var registerTime: String = ""     //注册时间
}