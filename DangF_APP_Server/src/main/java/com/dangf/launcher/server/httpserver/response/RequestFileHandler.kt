/*
 * Copyright © Yan Zhenjie. All Rights Reserved
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.dangf.launcher.server.httpserver.response

import com.dangf.launcher.server.app.V
import com.yanzhenjie.andserver.RequestHandler
import com.yanzhenjie.andserver.util.HttpRequestParser
import org.apache.http.HttpException
import org.apache.http.HttpRequest
import org.apache.http.HttpResponse
import org.apache.http.entity.FileEntity
import org.apache.http.entity.StringEntity
import org.apache.http.protocol.HttpContext
import java.io.File
import java.io.IOException
import java.net.URLDecoder

/**
 * http://192.168.1.103:8080/?file=文件名
 */
class RequestFileHandler : RequestHandler {

    @Throws(HttpException::class, IOException::class)
    override fun handle(request: HttpRequest, response: HttpResponse, context: HttpContext) {
        V.printLog("HTTP服务", "收到请求：${request.requestLine.uri}")
        // You can according to the client param can also be downloaded.
        val params = HttpRequestParser.parse(request)
        val path = URLDecoder.decode(params["name"], "utf-8")

        val file = File(V.SOFT_DATA_PATH + "/" + path)
        if (file.exists()) {
            response.setStatusCode(200)
            val contentLength = file.length()
            response.setHeader("ContentLength", java.lang.Long.toString(contentLength))
            response.entity = FileEntity(file, HttpRequestParser.getMimeType(file.name))
        } else {
            response.setStatusCode(404)
            response.entity = StringEntity("")
        }
    }


}
