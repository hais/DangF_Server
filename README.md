# DangF_Server

#### 项目介绍
计划开设一个“手机网吧”所做的准备工作，定制了手机的rec、以及Android系统，使得用户不能进入刷机模式对系统进行破坏。
该系统可用管理端进行上下机、加钱结账、增删查改游戏、展示广告、在线点餐等功能，后期还计划加入（游戏挂机脚本）。

#### 软件架构
基于有root权限的Android7.1手机

#### 软件图片
![Image text](https://gitee.com/hais/DangF_Server/raw/master/doc_image.png)

