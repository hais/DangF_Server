package com.dangf.launcher.server.pulgin.soft

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.dangf.launcher.server.R

/**
 * Created by hais1992 on 2017/9/29.
 */
class SoftAdapter(var list: List<Soft>) : BaseQuickAdapter<Soft, BaseViewHolder>(R.layout.soft_manage_fragment_item, list) {
    override fun convert(helper: BaseViewHolder?, item: Soft?) {
        val sort = (list.indexOf(item) + 1)
        helper?.setText(R.id.text_name, item?.name)
        helper?.setText(R.id.text_version, item?.version)
        helper?.setText(R.id.text_status, item?.status)
        helper?.setText(R.id.text_package, item?.packageName)
        helper?.setText(R.id.text_sort, sort.toString())
        if (sort % 2 == 0) helper?.setBackgroundColor(R.id.layout_item, (0xFFF5F8FA).toInt())
        else helper?.setBackgroundRes(R.id.layout_item, R.drawable.background_stroke_solid_icons_secondary)
    }
}