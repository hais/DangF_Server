package com.dangf.launcher.server.pulgin.machine

import org.litepal.crud.DataSupport

/**
 * 上机日记
 * Created by hais1992 on 2017/9/27.
 */
data class UsageLog(
        var machineCode: String? = null, //机器ID
        var machineName: String? = null, //机器ID
        var startTime: Long = 0, //上机时间
        var endTime: Long = 0, //下机时间
        var money: Double = 0.0, //总收入金钱
        var process: String = "", //进程
        var saleTime: Long = 0, //优惠时间
        var status: Long = 0      //上机状态
) : DataSupport()