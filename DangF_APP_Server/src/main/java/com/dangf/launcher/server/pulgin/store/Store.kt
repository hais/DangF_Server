package com.dangf.launcher.server.pulgin.store

import org.litepal.crud.DataSupport

/**
 * 店铺
 * Created by hais1992 on 2017/9/27.
 */
data class Store(
        var name: String = "",        //商店名称
        var username: String = "",    //用户名
        var password: String = "",    //管理密码
        var wallpaper: String = "",   //壁纸
        var timePrice: Int = 0       //一块钱可以上机的时间
) : DataSupport()