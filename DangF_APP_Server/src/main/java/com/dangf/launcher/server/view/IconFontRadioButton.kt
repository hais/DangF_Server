package com.dangf.launcher.server.view

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet

/**
 * Created by hais1992 on 2017/10/1.
 */

class IconFontRadioButton : android.support.v7.widget.AppCompatRadioButton {
    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    private fun init(context: Context) {
        //加载字体文件
        val typeface = IconFontTypeFace.getTypeface(context)
        this.typeface = typeface
        //去掉padding,这样iconfont和普通字体容易对齐
        includeFontPadding = false
    }
}
