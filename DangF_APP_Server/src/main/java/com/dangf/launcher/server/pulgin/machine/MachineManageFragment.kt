package com.dangf.launcher.server.pulgin.machine

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.dangf.launcher.server.R
import com.dangf.launcher.server.app.BaseFragment
import kotlinx.android.synthetic.main.machine_manage_fragment.*
import org.litepal.crud.DataSupport

/**
 * 机器管理
 * Created by hais1992 on 2017/9/27.
 */
class MachineManageFragment : BaseFragment(R.layout.machine_manage_fragment), BaseQuickAdapter.OnItemClickListener {
    private val log_list: MutableList<UsageLog> = arrayListOf()
    override fun onInitViewsAndData(v: View?) {
        val machine_list = DataSupport.findAll(Machine::class.java)
        machine_list.forEach {
            var log: UsageLog? = null
            try {
                log = DataSupport.where(" machineCode = '${it.code}' ").findLast(UsageLog::class.java)
            } catch (e: Exception) {
                log = UsageLog()
                log.machineCode = it.code
                log.machineName = it.name
            } finally {
                log_list.add(log!!)
            }
        }

        val adpaert = MachineAdapter(log_list)
        recycler_machine.layoutManager = LinearLayoutManager(activity)
        recycler_machine.adapter = adpaert
        adpaert.onItemClickListener = this

    }

    override fun onItemClick(adapter: BaseQuickAdapter<*, *>?, view: View?, position: Int) {

    }
}