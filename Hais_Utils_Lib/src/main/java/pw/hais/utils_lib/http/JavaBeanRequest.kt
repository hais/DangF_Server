package com.dangf.app.launcher.http

import com.alibaba.fastjson.JSON
import com.yanzhenjie.nohttp.Headers
import com.yanzhenjie.nohttp.RequestMethod
import com.yanzhenjie.nohttp.rest.RestRequest
import com.yanzhenjie.nohttp.rest.StringRequest


/**
 *
 * 自定义JavaBean请求。
 * Created by Yan Zhenjie on 2016/10/15.
 */
class JavaBeanRequest<T>(url: String, requestMethod: RequestMethod, private val clazz: Class<T>) : RestRequest<T>(url, requestMethod) {

    @Throws(Exception::class)
    override fun parseResponse(responseHeaders: Headers, responseBody: ByteArray): T {
        val response = StringRequest.parseResponseString(responseHeaders, responseBody)
        // 这里如果数据格式错误，或者解析失败，会在失败的回调方法中返回 ParseError 异常。
        return JSON.parseObject(response, clazz)
    }
}