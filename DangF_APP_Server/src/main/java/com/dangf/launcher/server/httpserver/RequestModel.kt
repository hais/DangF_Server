package com.dangf.launcher.server.httpserver

/**
 * Created by hais1992 on 2017/9/29.
 */
data class RequestModel<T>(var status: Int, var message: String, var result: T)