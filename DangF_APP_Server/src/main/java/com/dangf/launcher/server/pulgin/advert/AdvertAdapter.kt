package com.dangf.launcher.server.pulgin.advert

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.dangf.launcher.server.R

/**
 * Created by hais1992 on 2017/9/29.
 */
class AdvertAdapter(var list: List<Advert>) : BaseQuickAdapter<Advert, BaseViewHolder>(R.layout.advert_manage_fragment_item, list) {
    override fun convert(helper: BaseViewHolder?, item: Advert?) {
        val sort = (list.indexOf(item) + 1)
        helper?.setText(R.id.text_status, item?.status)
        helper?.setText(R.id.text_title, item?.title)
        helper?.setText(R.id.text_position, item?.position)
        helper?.setText(R.id.text_type, item?.type)
        helper?.setText(R.id.text_sort, sort.toString())
        if (sort % 2 == 0) helper?.setBackgroundColor(R.id.layout_item, (0xFFF5F8FA).toInt())
        else helper?.setBackgroundRes(R.id.layout_item, R.drawable.background_stroke_solid_icons_secondary)
    }
}