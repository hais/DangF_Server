package pw.hais.utils_lib.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

/**
 * AppAdapter
 * 简介: 实现简单的列表形式,可以帮助你更快捷的开发,无须再重复实现内容
 * Created by Single on 15-7-18.
 *
 * @version 1.0
 */
public abstract class AppAdapter<E, T extends AppAdapter.ViewHolder> extends android.widget.BaseAdapter {

    private int layout;
    private Class<T> mHolderClass;
    public List<E> mList;
    private E[] mArray;

    public AppAdapter(List<E> mList, int layout, Class<T> mHolderClass) {
        this.layout = layout;
        this.mList = mList;
        this.mHolderClass = mHolderClass;
    }

    public AppAdapter(E[] mArray, int layout, Class<T> mHolderClass) {
        this.layout = layout;
        this.mArray = mArray;
        this.mHolderClass = mHolderClass;
    }

    private AppAdapter() {
    }

    public void updateDataSet(List<E> mList) {
        this.mList = mList;
        this.notifyDataSetChanged();
    }

    public void updateDataSet(E[] mArray) {
        this.mArray = mArray;
        this.notifyDataSetChanged();
    }

    public void appendData(List<E> mList) {
        this.mList.addAll(mList);
        this.notifyDataSetChanged();
    }

    public void appendData(E[] mArray) {
        int newLength = this.mArray.length + mArray.length;
        E[] mNewArray = Arrays.copyOf(this.mArray, newLength);
        for (int i = this.mArray.length - 1; i < newLength; i++) {
            mNewArray[i] = mArray[i - mArray.length - 1];
        }
        this.mArray = mNewArray;
        this.notifyDataSetChanged();
    }

    public void appendData(E mItem) {
        if (this.mList != null) {
            this.mList.add(mItem);
        }

        if (this.mArray != null) {
            this.mArray = Arrays.copyOf(this.mArray, this.mArray.length + 1);
            this.mArray[this.mArray.length - 1] = mItem;
        }
        this.notifyDataSetChanged();
    }

    public void removeListData() {

        if (this.mList != null) {
            this.mList.clear();
        }
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {

        if (this.mList != null) {
            return this.mList.size();
        }

        if (this.mArray != null) {
            return this.mArray.length;
        }

        return 0;
    }

    @Override
    public E getItem(int position) {

        if (this.mList != null) {
            return (E) this.mList.get(position);
        }

        if (this.mArray != null) {
            return (E) this.mArray[position];
        }

        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = loadingConvertView(parent.getContext(), convertView, layout, mHolderClass);
        ViewHolder mHolder = (ViewHolder) convertView.getTag();
        mHolder.itemView = convertView;
        onBindView(position, (T) mHolder, (E) getItem(position));
        return convertView;
    }

    public void replaceListItem(int position, E mItem) {
        mList.set(position, mItem);
        this.notifyDataSetChanged();
    }

    /**
     * 进行数据绑定
     *
     * @param position
     * @param mViewHolder
     * @param mItem
     */
    public abstract void onBindView(int position, T mViewHolder, E mItem);

    /**
     * ViewHolder
     */
    public static class ViewHolder {
        public View itemView;
    }


    /**
     * 利用反射机制实例化对象,主要运用在Adapter复用
     *
     * @param mContext    上下文对象
     * @param convertView 复用的view <b>(Adapter getView 方法中的convertView)</b>
     * @param layoutId    实例化布局ID
     * @param cls         ViewHolder <b>(你的实体类)</b>
     * @return 装载后的view <b>(直接返回给getView)</b>
     */
    public static View loadingConvertView(Context mContext, View convertView, int layoutId, Class<?> cls) {
        try {
            if (null == convertView) {
                convertView = LayoutInflater.from(mContext).inflate(layoutId, null);
                Object obj = cls.newInstance();
                Field[] mFields = obj.getClass().getDeclaredFields();

                for (Field mField : mFields) {
                    //得到资源ID
                    int resourceId = mContext.getResources().getIdentifier(mField.getName(), "id", mContext.getApplicationContext().getPackageName());
                    //允许访问私有属性
                    mField.setAccessible(true);
                    //保存实例化后的资源
                    mField.set(obj, convertView.findViewById(resourceId));
                }
                convertView.setTag(obj);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return convertView;
    }


}
