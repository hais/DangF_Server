package com.dangf.launcher.server.app

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by hais1992 on 2017/9/27.
 */
abstract class BaseFragment(var layout: Int) : Fragment() {
    private var v: View? = null
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        v = inflater!!.inflate(layout, null)
        return v
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onInitViewsAndData(v)
    }

    abstract fun onInitViewsAndData(v: View?)
}