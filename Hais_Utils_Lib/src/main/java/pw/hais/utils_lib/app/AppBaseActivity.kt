package pw.hais.utils_lib.app

import android.os.Bundle
import android.support.v4.app.FragmentActivity
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import pw.hais.utils_lib.entity.EventT
import pw.hais.utils_lib.utils.L


/**
 * Created by hais1992 on 2017/5/19.
 */
abstract class AppBaseActivity(private var layoutId: Int) : FragmentActivity() {
    var tag: String? = null
    var context: AppBaseActivity? = null
    private var isDrawFinish = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        context = this
        tag = javaClass.simpleName

        onInitViewsAndData()
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus && !isDrawFinish) {
            isDrawFinish = true
            onDrawFinish()
        }
    }

    open fun onDrawFinish() {

    }

    abstract fun onInitViewsAndData()

    override fun onPause() {
        super.onPause()
        if (EventBus.getDefault().isRegistered(this)) EventBus.getDefault().unregister(this)
    }

    override fun onResume() {
        super.onResume()
        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN) //收到信息
    fun onEventMainThread(eventT: EventT) {
        L.i("收到事件->" + eventT.type)
    }
}