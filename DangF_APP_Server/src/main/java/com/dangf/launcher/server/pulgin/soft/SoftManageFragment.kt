package com.dangf.launcher.server.pulgin.soft

import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.dangf.app.launcher.entity.event.MainEType
import com.dangf.app.launcher.entity.event.MainEvent
import com.dangf.launcher.server.R
import com.dangf.launcher.server.app.BaseFragment
import com.dangf.launcher.server.app.V
import kotlinx.android.synthetic.main.soft_manage_fragment.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.litepal.crud.DataSupport
import pw.hais.utils_lib.utils.AppUtils
import pw.hais.utils_lib.utils.L

/**
 * 软件管理
 * Created by hais1992 on 2017/9/27.
 */
class SoftManageFragment : BaseFragment(R.layout.soft_manage_fragment), BaseQuickAdapter.OnItemClickListener, View.OnClickListener {
    private val softEditWindows by lazy { SoftEditWindows(this) }
    private var softType: String? = null

    override fun onInitViewsAndData(v: View?) {
        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this)
        text_soft_user_list.setOnClickListener(this)
        text_soft_admin_list.setOnClickListener(this)
        text_soft_post.setOnClickListener { showFileChooser() }
        text_soft_user_list.performClick()
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.text_soft_user_list, R.id.text_soft_admin_list -> setItemColor(p0 as TextView)
        }
    }

    fun setItemColor(view: TextView?) {
        softType = view?.text.toString()
        text_user_lable.visibility = View.GONE
        text_admin_lable.visibility = View.GONE
        //复原字体颜色
        text_soft_user_list.setTextColor(activity.resources.getColor(R.color.app_primary_text))
        text_soft_admin_list.setTextColor(activity.resources.getColor(R.color.app_primary_text))
        //复原背景色
        text_soft_user_list.setBackgroundColor(activity.resources.getColor(R.color.app_primary_light))
        text_soft_admin_list.setBackgroundColor(activity.resources.getColor(R.color.app_primary_light))

        //根据要求显示
        view?.setTextColor(activity.resources.getColor(R.color.app_icons))
        view?.setBackgroundColor(activity.resources.getColor(R.color.app_primary_dark))
        when (view?.id) {
            R.id.text_soft_user_list -> text_user_lable.visibility = View.VISIBLE
            R.id.text_soft_admin_list -> text_admin_lable.visibility = View.VISIBLE
        }
        onRefreshData()
    }


    override fun onItemClick(adapter: BaseQuickAdapter<*, *>?, view: View?, position: Int) {
        val item = adapter?.getItem(position) as Soft
        softEditWindows.showContent(item)
    }

    //上传软件
    private fun showFileChooser() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"//设置类型，我这里是任意类型，任意后缀的可以这样写。
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        activity.startActivityForResult(intent, V.SOFE_EDIT_FILE_SELECT_APK_CODE)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEventMainThreadx(event: MainEvent) {
        when (event.type) {
            MainEType.FileSelectResule -> {
                val url = event.obj.toString()
                if (url.contains(".apk")) {   //软件选择成功
                    val appInfo = AppUtils.getAppInfoByFile(url)
                    if (appInfo == null) L.showShort("软件解析出错，请重新选择APK文件！")
                    else {
                        val soft = Soft(appInfo.name, appInfo.packageName, "", "", "", appInfo.packagePath, "", appInfo.versionName, "无")
                        softEditWindows.showContent(soft, appInfo.icon)
                    }
                }
            }
        }
    }

    //刷新数据
    fun onRefreshData() {
        val type = if (softType!!.contains("用户")) "用户软件" else "管理工具"
        val data_list = DataSupport.where("type = '$type'").find(Soft::class.java)
        val adpaert = SoftAdapter(data_list)
        recycler_soft.layoutManager = LinearLayoutManager(activity)
        recycler_soft.adapter = adpaert
        adpaert.onItemClickListener = this
    }

}