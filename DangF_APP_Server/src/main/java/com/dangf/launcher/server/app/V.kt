package com.dangf.launcher.server.app

import android.os.Environment
import com.dangf.app.launcher.entity.event.MainEType
import com.dangf.app.launcher.entity.event.MainEvent
import com.dangf.launcher.server.pulgin.store.Store
import com.yanzhenjie.nohttp.tools.NetUtils
import pw.hais.utils_lib.utils.DateUtils
import pw.hais.utils_lib.utils.L

/**
 * Created by hais1992 on 2017/9/27.
 */
object V {
    var store: Store? = null                                        //商家信息
    val runLog = StringBuffer()                                     //运行日记
    val DATA_PATH = "DangF/Server/Web"                              //数据保存路径
    val SD_PATH = Environment.getExternalStorageDirectory().path    //SD卡根目录
    val SOFT_DATA_PATH = "$SD_PATH/$DATA_PATH/Softs"                //软件存放露姐

    val HTTP_SERVER_IP  = NetUtils.getLocalIPAddress()      //本机IP地址
    val HTTP_SERVER_PORT = 8080                             //HTTP网站端口
    val SOCKECT_SERVER_PORT = 40380                         //Mina服务端口

    val SOFE_EDIT_FILE_SELECT_APK_CODE = 123            //选择软件回调判断

    fun printLog(tag: String, str: String) {
        val log = "${DateUtils.getStrTime("HH:mm:ss")}　$tag　$str\n"
        runLog.insert(0, log)
        L.e(str, "RunLog-$tag")
        MainEvent.post(MainEType.RefreshLog, log)
    }
}