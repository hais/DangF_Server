package pw.hais.utils_lib.mina

import pw.hais.utils_lib.utils.L

/**
 * Created by hais1992 on 2017/9/26.
 */
interface OnSockectListener {

    fun onSessionOpened() {
        L.e("onSessionOpened", "Mina")
    }

    fun onSessionClosed() {
        L.e("onSessionClosed", "Mina")
    }

    fun onMessageReceived(message: String?)
}