package com.dangf.launcher.server.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.dangf.app.launcher.entity.event.IOEType
import com.dangf.app.launcher.entity.event.IOEvent
import com.dangf.launcher.server.app.V
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import pw.hais.utils_lib.mina.OnSockectListener
import pw.hais.utils_lib.mina.SockectConfig
import pw.hais.utils_lib.mina.server.MinaServer
import pw.hais.utils_lib.utils.L

/**
 * Created by hais1992 on 2017/9/29.
 */
class SockectService : Service(), OnSockectListener {
    private val TAG = "MINA服务"

    override fun onSessionOpened() {
        V.printLog(TAG, "Session被打开")
    }

    override fun onSessionClosed() {
        V.printLog(TAG, "Session被关闭")
    }

    override fun onMessageReceived(message: String?) {
        V.printLog(TAG, "收到消息：" + message)
    }

    val sockectServer = MinaServer(SockectConfig(null, V.SOCKECT_SERVER_PORT), this)

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        V.printLog(TAG, "已启动,监听端口 ${V.SOCKECT_SERVER_PORT}")
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
        stopSelf()
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    fun onEventMainThreadx(event: IOEvent) {
        L.i("SockectService 收到 BACKGROUND 事件：" + event.type + " -> " + event.obj)
        when (event.type) {
            IOEType.SendSockect -> {
                V.printLog(TAG, "发送信息：" + event.obj)
                sockectServer.sendMessage(event.obj.toString())
            }
        }
    }
}

