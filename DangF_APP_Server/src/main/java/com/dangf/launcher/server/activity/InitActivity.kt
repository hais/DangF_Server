package com.dangf.launcher.server.activity

import android.content.Intent
import com.dangf.launcher.server.R
import com.dangf.launcher.server.app.BaseActivity
import com.dangf.launcher.server.app.V
import com.dangf.launcher.server.pulgin.store.Store
import org.litepal.crud.DataSupport
import pw.hais.utils_lib.utils.ShellUtils


/**
 * 程序初始化
 * Created by hais1992 on 2017/9/27.
 */
class InitActivity : BaseActivity(R.layout.activity_init) {
    override fun onInitViewsAndData() {
        V.store = DataSupport.findFirst(Store::class.java)
        if (V.store == null) {
            ShellUtils.execCmd("mkdir -p ${V.SOFT_DATA_PATH}",false)
            startActivity(Intent(context, InstallActivity::class.java))
        } else {
            startActivity(Intent(context, MainActivity::class.java))
        }
        finish()
    }
}