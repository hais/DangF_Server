package com.dangf.launcher.server.activity

import android.app.Activity
import android.content.Intent
import android.view.View
import com.dangf.app.launcher.entity.event.MainEType
import com.dangf.app.launcher.entity.event.MainEvent
import com.dangf.launcher.server.R
import com.dangf.launcher.server.app.BaseActivity
import com.dangf.launcher.server.app.V.SOFE_EDIT_FILE_SELECT_APK_CODE
import com.dangf.launcher.server.pulgin.advert.AdvertManageFragment
import com.dangf.launcher.server.pulgin.machine.MachineManageFragment
import com.dangf.launcher.server.pulgin.soft.SoftManageFragment
import com.dangf.launcher.server.pulgin.store.StoreInfoManageFragment
import com.dangf.launcher.server.service.HttpService
import com.dangf.launcher.server.service.SockectService
import com.dangf.launcher.server.utils.FragmentUtils
import com.dangf.launcher.server.utils.UriToPathUtils
import kotlinx.android.synthetic.main.activity_main.*


//管理首页
class MainActivity : BaseActivity(R.layout.activity_main), View.OnClickListener {
    private val machineManageFragment by lazy { MachineManageFragment() }
    private val softsManageFragment by lazy { SoftManageFragment() }
    private val advertManageFragment by lazy { AdvertManageFragment() }
    private val systemManageFragment by lazy { StoreInfoManageFragment() }

    override fun onInitViewsAndData() {
        startService(Intent(context, HttpService::class.java))
        startService(Intent(context, SockectService::class.java))
        layout_machine.setOnClickListener(this)
        layout_softs.setOnClickListener(this)
        layout_advert.setOnClickListener(this)
        layout_setting.setOnClickListener(this)
    }

    override fun onDrawFinish() {
        super.onDrawFinish()
        layout_machine.performClick()
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.layout_machine -> {
                FragmentUtils.replaceFragment(supportFragmentManager, machineManageFragment, R.id.frame_main, false)
            }
            R.id.layout_softs -> {
                FragmentUtils.replaceFragment(supportFragmentManager, softsManageFragment, R.id.frame_main, false)
            }
            R.id.layout_advert -> {
                FragmentUtils.replaceFragment(supportFragmentManager, advertManageFragment, R.id.frame_main, false)
            }
            R.id.layout_setting -> {
                FragmentUtils.replaceFragment(supportFragmentManager, systemManageFragment, R.id.frame_main, false)
            }
        }

        layout_machine?.setBackgroundColor(context!!.resources.getColor(R.color.app_primary))
        layout_softs?.setBackgroundColor(context!!.resources.getColor(R.color.app_primary))
        layout_advert?.setBackgroundColor(context!!.resources.getColor(R.color.app_primary))
        layout_setting?.setBackgroundColor(context!!.resources.getColor(R.color.app_primary))
        p0?.setBackgroundColor(context!!.resources.getColor(R.color.app_primary_dark))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            SOFE_EDIT_FILE_SELECT_APK_CODE -> if (resultCode === Activity.RESULT_OK) {
                val path = UriToPathUtils.getRealFilePath(this,data?.data!!)
//                val path = data?.data?.path?.replace("external", "sdcard")
                MainEvent.post(MainEType.FileSelectResule, path + "")
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

}
